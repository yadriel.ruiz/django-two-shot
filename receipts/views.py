from django.shortcuts import render, redirect, get_object_or_404, get_list_or_404
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import CreateReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.
@login_required
def show_receipts(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipt_list
    }
    return render(request, "receipts/receipts.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = CreateReceiptForm()
    context = {
        "form": form
    }
    return render(request, "receipts/create.html", context)

@login_required
def category_list(request):
    things = ExpenseCategory.objects.filter(owner=request.user)
    context = {
    "category_list": things
    }
    return render (request, "receipts/ExpenseCategory.html", context)

@login_required
def account_list(request):
    things = Account.objects.filter(owner=request.user)
    context = {
    "account_list": things
    }
    return render (request, "receipts/Account_list.html", context)

@login_required
def create_expense_category(request):
  if request.method == "POST":
    form = ExpenseCategoryForm(request.POST)
    if form.is_valid():
      model_instance = form.save(commit=False)
      model_instance.owner = request.user
      model_instance.save()
      return redirect("category_list")
  else:
    form = ExpenseCategoryForm()

  context = {
    "form": form
  }

  return render(request, "categories/create.html", context)

@login_required
def create_account(request):
  if request.method == "POST":
    form = AccountForm(request.POST)
    if form.is_valid():
      model_instance = form.save(commit=False)
      model_instance.owner = request.user
      model_instance.save()
      return redirect("account_list")
  else:
    form = AccountForm()

  context = {
    "form": form
  }

  return render(request, "accounts/create.html", context)